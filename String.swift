import Foundation

extension String {
	
	func extractNums() -> [Int] {
		
		let this = self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: " ")	
		var nums = [Int]()
		var newDigit: Bool = false
		var digit: Int = 0	
		for c in this.characters {
			if let d = Int(String(describing: c)) {
				if newDigit {
					digit = digit * 10 + d
				} else {
					newDigit = true
					digit = d
				}	
			} else {
				if newDigit {
					nums.append(digit)
				}
				newDigit = false
			}
		}
		return nums
	}
}





